package org.example;

import java.util.Set;

import org.example.dao.AuthorDao;
import org.example.dao.NewMovieDao;
import org.example.dao.OldMovieDao;
import org.example.dao.UserDao;
import org.example.models.AccountType;
import org.example.models.Author;
import org.example.models.Category;
import org.example.models.Movie;
import org.example.models.User;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //gui

        //init
//        final OldMovieDao movieDao = new OldMovieDao();
        final NewMovieDao movieDao = new NewMovieDao();

        final Movie drogaPrzezMeke = new Movie("Droga przez meke", 1992, 345346334L);
        final Movie savedMovie = movieDao.save(drogaPrzezMeke, Movie.class);
        final Movie movieFromDb = movieDao.getById(savedMovie.getId(), Movie.class);
        //nie ma wyjatku bo eager
        System.out.println(movieFromDb.getAuthor());
        //wyjatek bo lazy
//        System.out.println(movieFromDb.getCategories());

        final Movie moviesWithCategories = movieDao.getMoviesWithCategories(1L);
        System.out.println(moviesWithCategories.getCategories());

        System.out.println("MOVIE FROM DB WITH ID 1L");
        System.out.println(movieFromDb);

        movieFromDb.setTitle("Droga przez mękę");
        movieDao.update(movieFromDb);
        System.out.println("MOVIE AFTER UPDATE: " + movieDao.getById(savedMovie.getId(), Movie.class));

//        final Movie remove = movieDao.remove(savedMovie.getId());
//        System.out.println("WLASNIE USUNALES: " + remove);

        final AuthorDao authorDao = new AuthorDao();
        final Author author = new Author("Adam", "Mickiewicz", 28, movieFromDb);
        //tak mozna dodac film do autora
        author.setMovie(movieFromDb);
        final Author mickiewicz = authorDao.save(author, Author.class);
        //wyciagniecie z bazy danych
        //final Author mickiewiczFormDb = authorDao.getById(mickiewicz.getId(), Author.class);
        System.out.println(mickiewicz);

        System.out.println("USER!");
        final User user = new User("Adam", "Puso", "pusio");
        user.setAccountType(AccountType.SUPER_PREMIUM);
        final UserDao userDao = new UserDao();
        final User savedUser = userDao.save(user, User.class);
        userDao.save(new User("Bot1", "BotPasswrod", "bot"), User.class);

        movieFromDb.setUser(savedUser);
        movieDao.update(movieFromDb);

        System.out.println("PRINT ALL: !!!");
        userDao.getAll().forEach(System.out::println);

        HibernateFactory.getSessionFactory().close();
    }
}
