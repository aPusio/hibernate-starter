package org.example;

import java.util.List;
import java.util.Optional;

import org.example.dao.AuthorDao;
import org.example.dao.NewMovieDao;
import org.example.models.Author;
import org.example.models.Movie;
import org.example.models.PartialMovie;

public class QuerryExample {
	public static void main(String[] args) {
		final NewMovieDao newMovieDao = new NewMovieDao();

		final Movie firstMovie = newMovieDao.save(new Movie(
				"titanic"
				, 1222,
				1324234L),
			Movie.class);
		newMovieDao.save(new Movie(
				"titanic 2"
				, 1222,
				1324234L),
			Movie.class);

		final List<Movie> movies = newMovieDao.getMovieByName("titanic");
		movies.forEach(System.out::println);

//		final Optional<PartialMovie> partialMovie = newMovieDao.getPartialMovie(1L);
//		partialMovie.ifPresent(System.out::println);

		newMovieDao.persistanceContext(1L);
		System.out.println(newMovieDao.getById(1L, Movie.class));

		final AuthorDao authorDao = new AuthorDao();
		authorDao.save(
			new Author(
				"Roman",
				"NieRoman",
				12,
				null), Author.class);

		authorDao.getAllAuthorsUsingCriteriaQuerry()
					 .forEach(System.out::println);

		System.out.println("BY NAME!: ");
		System.out.println(authorDao.getAuthorByName("Roman"));
		HibernateFactory.getSessionFactory().close();
	}
}
