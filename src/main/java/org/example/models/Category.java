package org.example.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String alias;

	//TODO do potwierdzenia kolejnosc kolumn
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(
		name = "FILMY_KATEGORIE_TABELA_POSREDNIA",
		joinColumns = {@JoinColumn(name = "FILM_ID")},
		inverseJoinColumns = {@JoinColumn(name = "CATEGORIA_ID")}
	)
	private Set<Movie> movies;

	public Category() {
	}

	public Category(String name, String alias) {
		this.name = name;
		this.alias = alias;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Override
	public String toString() {
		return "Category{" +
				   "id=" + id +
				   ", name='" + name + '\'' +
				   ", alias='" + alias + '\'' +
				   '}';
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}
}
