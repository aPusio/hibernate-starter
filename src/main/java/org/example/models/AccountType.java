package org.example.models;

public enum AccountType {
	FREE,
	STANDARD,
	PREMIUM,
	SUPER_PREMIUM
}
