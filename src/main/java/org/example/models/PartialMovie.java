package org.example.models;

public class PartialMovie {
	private String title;
	private int publicationYear;

	public PartialMovie() {
	}

	public PartialMovie(String title, int publicationYear) {
		this.title = title;
		this.publicationYear = publicationYear;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPublicationYear() {
		return publicationYear;
	}

	public void setPublicationYear(int publicationYear) {
		this.publicationYear = publicationYear;
	}

	@Override
	public String toString() {
		return "PartialMovie{" +
				   "title='" + title + '\'' +
				   ", publicationYear=" + publicationYear +
				   '}';
	}
}
