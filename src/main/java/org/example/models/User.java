package org.example.models;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.pl.NIP;
import org.hibernate.validator.constraints.pl.PESEL;


@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
//	@Email
	@NotNull
	private String username;
	@NotNull
	private String password;
	@Column(unique = true)
	@NotNull(message = "null nie jest mile widziany")
	@Size(min = 2, max = 12)
	private String login;

	@OneToMany(mappedBy = "user")
	private Set<Movie> movies;

	@Embedded
	private Address address;

	@Enumerated(value = EnumType.STRING)
	private AccountType accountType;

	public User(){}

	public User(String username, String password, String login) {
		this.username = username;
		this.password = password;
		this.login = login;
	}

	@Override
	public String toString() {
		return "User{" +
				   "id=" + id +
				   ", username='" + username + '\'' +
				   ", password='" + password + '\'' +
				   ", login='" + login + '\'' +
				   '}';
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
}
