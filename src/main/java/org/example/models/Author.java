package org.example.models;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Author {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String surname;
	private int age;

	@Embedded
	private Address address;

	@OneToOne
	private Movie movie;

	public Author(){}

	public Author(String name, String surname, int age, Movie movie) {
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.movie = movie;
	}

	@Override
	public String toString() {
		return "Author{" +
				   "id=" + id +
				   ", name='" + name + '\'' +
				   ", surname='" + surname + '\'' +
				   ", age=" + age +
				   ", movie=" + movie +
				   '}';
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
