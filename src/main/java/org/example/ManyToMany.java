package org.example;

import java.util.Set;

import org.example.dao.CategoryDao;
import org.example.dao.NewMovieDao;
import org.example.models.Category;
import org.example.models.Movie;

public class ManyToMany {
	public static void main(String[] args) {
		final NewMovieDao movieDao = new NewMovieDao();
		final CategoryDao categoryDao = new CategoryDao();

		final Movie movie = new Movie(
			"Szybcy i wscielki 55",
			3234,
			35453453L);
		final Category category = new Category(
			"HORROR",
			"HRR");

		movie.setCategories(Set.of(category));
		category.setMovies(Set.of(movie));

		//film sie zapisze razem z kategoria poniewaz uzylismy cascade
//		movieDao.save(movie, Movie.class);
		categoryDao.save(category, Category.class);

		HibernateFactory.getSessionFactory().close();
	}
}
