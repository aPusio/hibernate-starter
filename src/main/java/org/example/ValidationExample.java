package org.example;

import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.example.dao.UserDao;
import org.example.models.User;
import org.hibernate.Hibernate;

public class ValidationExample {

	public static void main(String[] args) {
		final UserDao userDao = new UserDao();
		final User user = new User(
			"sampleUser",
			"samplePassword",
			"2");

//		final Validator validator = Validation.buildDefaultValidatorFactory()
//			.getValidator();
//		validator.validate(user, User.class);

		userDao.save(user, User.class);
		HibernateFactory.getSessionFactory().close();
	}

	public static void validate(@Valid User user){
		System.out.println("HURA ! " + user);
	}
}
