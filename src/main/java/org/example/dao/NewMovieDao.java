package org.example.dao;

import java.util.List;
import java.util.Optional;

import org.example.HibernateFactory;
import org.example.models.Movie;
import org.example.models.PartialMovie;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class NewMovieDao extends EntityDao<Movie> {
	public List<Movie> getMovieByName(String functionTitle){
		final Session session = HibernateFactory.getSessionFactory()
			.openSession();
		final Query<Movie> query = session
			.createQuery("From Movie as M where M.title = :querryTitle", Movie.class);
		query.setParameter("querryTitle", functionTitle);
		final List<Movie> resultList = query.getResultList();
		session.close();
		return resultList;
	}

	public Optional<PartialMovie> getPartialMovie(Long id){
		final Session session = HibernateFactory.getSessionFactory().openSession();
		//TODO konstruktor
		final Query<PartialMovie> query = session.createQuery(
			"SELECT M.title, M.publicationYear FROM Movie M WHERE M.id = :id", PartialMovie.class);
		query.setParameter("id", id);
		final Optional<PartialMovie> first = query.stream().findFirst();
		session.close();
		return first;
	}

	public Movie getMoviesWithCategories(Long id){
		final Session session = HibernateFactory.getSessionFactory().openSession();
		final Movie movie = session.get(Movie.class, id);
		Hibernate.initialize(movie.getCategories());
		session.close();
		return movie;
	}

	public void persistanceContext(Long id){
		final Session session = HibernateFactory.getSessionFactory().openSession();
		final Movie movie = session.get(Movie.class, id);
		final Transaction transaction = session.beginTransaction();
		movie.setTitle("UUUUUU");
		//session.save(movie);
		transaction.commit();
		session.close();
	}
}
