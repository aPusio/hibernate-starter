package org.example.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.example.HibernateFactory;
import org.example.models.Author;
import org.hibernate.Session;

public class AuthorDao extends EntityDao<Author> {

	public List<Author> getAllAuthorsUsingCriteriaQuerry() {
		Session session = HibernateFactory.getSessionFactory().openSession();
		//builder - dp twprzenia zapytania
		CriteriaBuilder cb = session.getCriteriaBuilder();
		//tworzymy szkielet dla querry z typem zwracanym
		CriteriaQuery<Author> query = cb.createQuery(Author.class);
		//warunek from
		Root<Author> from = query.from(Author.class);
		//select
		CriteriaQuery<Author> select = query.select(from);

		//wywolanie
		final List<Author> resultList = session.createQuery(select).getResultList();
		session.close();

		return resultList;
	}

	public List<Author> getAllAuthorsUsingCriteriaQuerryShort() {
		Session session = HibernateFactory.getSessionFactory().openSession();
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Author> query = cb.createQuery(Author.class);

		final List<Author> resultList = session.createQuery(
				query.select(query.from(Author.class)))
			.getResultList();

		session.close();

		return resultList;
	}

	public List<Author> getAuthorByName(String name) {
		final Session session = HibernateFactory.getSessionFactory().openSession();
		final CriteriaBuilder cb = session.getCriteriaBuilder();
		final CriteriaQuery<Author> query = cb.createQuery(Author.class);

		final Root<Author> from = query.from(Author.class);
		final List<Author> resultList = session.createQuery(
				query.select(from).where(cb.equal(from.get("name"), name)))
			.getResultList();
		session.close();
		return resultList;
	}
}
