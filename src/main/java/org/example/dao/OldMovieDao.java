package org.example.dao;

import java.util.List;

import org.example.HibernateFactory;
import org.example.models.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class OldMovieDao {
	public Movie save(Movie movie){
		final SessionFactory sessionFactory = HibernateFactory.getSessionFactory();
		final Session session = sessionFactory.openSession();
		final Transaction transaction = session.beginTransaction();
		final Long savedId = (Long)session.save(movie);
		transaction.commit();
		final Movie movieFromDb = session.get(Movie.class, savedId);
		session.close();
		return movieFromDb;
	}

	public Movie getById(Long id){
		final Session session = HibernateFactory.getSessionFactory()
			.openSession();
		Movie movie = session.get(Movie.class, id);
		session.close();
		return movie;
	}

	public Movie remove(Long id){
		final Session session = HibernateFactory.getSessionFactory()
			.openSession();
		final Transaction transaction = session.beginTransaction();
		final Movie movie = session.get(Movie.class, id);
		session.remove(movie);
		transaction.commit();
		session.close();
		return movie;
	}

	public void update(Movie movie){
		final Session session = HibernateFactory.getSessionFactory()
			.openSession();
		final Transaction transaction = session.beginTransaction();
		session.update(movie);
		transaction.commit();
		session.close();
	}

}
