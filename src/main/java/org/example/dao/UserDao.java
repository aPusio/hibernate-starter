package org.example.dao;

import java.util.List;

import org.example.HibernateFactory;
import org.example.models.User;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class UserDao extends EntityDao<User> {

	public List<User> getAll() {
		final Session session = HibernateFactory.getSessionFactory()
			.openSession();
		final Query<User> getAllQuerry = session.createQuery("From User", User.class);
		final List<User> resultList = getAllQuerry.getResultList();
		session.close();
		return resultList;
	}

}
